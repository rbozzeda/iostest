# Teste Prático iOS

Este projeto é um teste prático em iOS realizado por Renan Bozzeda, para a FourSys / Bradesco.

### Techs
  - Moya
  - Alamofire
  - RxSwift

### Arquitetura
  - Clean-Swift

### Instalação
Este projeto utiliza dependencias do CocoaPods.
Após clonar o projeto, você precisa instalar os Pods. Para isso abra o terminar e vá até a pasta do projeto e execute o seguinte comando:

```sh
$ pod install
```

Após isso, abrir o arquivo iOSTest.xcworkspace.
