//
//  Movie.swift
//  iOSTest
//

import Foundation

public struct Movie: Codable {
    
    public let id:              Int?
    public let title:           String?
    public let vote_average:    Double?
    public let poster_path:     String?
    public let overview:        String?
    public let backdrop_path:   String?
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case title              = "title"
        case vote_average
        case overview           = "overview"
        case poster_path        = "poster_path"
        case backdrop_path      = "backdrop_path"
    }
}
