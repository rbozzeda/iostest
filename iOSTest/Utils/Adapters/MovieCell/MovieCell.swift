//
//  MovieCell.swift
//  iOSTest
//
//  Created by Renan Santana Bozzeda on 04/06/18.
//  Copyright © 2018 Renan Bozzeda. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class MovieCell: UITableViewCell {

    //MARK: - Outlets & Vars
    @IBOutlet var poster:   UIImageView!
    @IBOutlet var title:    UILabel!
    @IBOutlet var overview: UILabel!
    
    var movie : Movie? {
        didSet {
            
            self.title.text = movie?.title
            
            self.overview.text = movie?.overview
            self.overview.sizeToFit()
            
            // set image
            if movie?.poster_path != nil {
                let imageURL = URL(string: Constants.imageURL + (movie?.poster_path)!)!
                self.poster.af_setImage(withURL: imageURL, placeholderImage: UIImage(), imageTransition: .crossDissolve(0.1))
            } else {
                self.poster.image = UIImage()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
