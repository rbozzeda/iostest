//
//  UIButton+CustomMethods.swift
//  iOSTest
//

import Foundation
import UIKit

@IBDesignable
class CustomButton: UIButton
{
    @IBInspectable
    public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    public var colorForHighlighted: UIColor = .lightGray {
        didSet {
            setBackgroundColor(colorForHighlighted, forState: .highlighted)
            setBackgroundColor(colorForHighlighted, forState: .selected)
        }
    }
    
    @IBInspectable
    public var colorForNormal: UIColor = .darkGray {
        didSet {
            setBackgroundColor(colorForNormal, forState: .normal)
        }
    }
    
    @IBInspectable
    public var colorForDisabled: UIColor = .lightGray {
        didSet {
            setBackgroundColor(colorForDisabled, forState: .disabled)
        }
    }
    
    
    @IBInspectable
    public var titleColorForHighlighted: UIColor? {
        get {
            return titleColor(for: .highlighted)
        }
        set {
            setTitleColor(newValue, for: .highlighted)
            setTitleColor(newValue, for: .selected)
        }
    }
    
    @IBInspectable
    public var titleColorForNormal: UIColor? {
        get {
            return titleColor(for: .normal)
        }
        set {
            setTitleColor(newValue, for: .normal)
        }
    }
    
    @IBInspectable
    public var titleColorForDisabled: UIColor? {
        get {
            return titleColor(for: .disabled)
        }
        set {
            setTitleColor(newValue, for: .disabled)
        }
    }
    
    override var isEnabled: Bool
        {
        didSet {
            if isEnabled {
                
                backgroundColor = colorForNormal
                setBackgroundColor(colorForNormal, forState: .normal)
                
            } else {
                backgroundColor = UIColor.lightGray
                setTitleColor(titleColorForDisabled, for: .disabled)
                setBackgroundColor(UIColor.lightGray, forState: .normal)
            }
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                
                animateScaleButton(scale: 0.95)
                backgroundColor = colorForHighlighted
                setTitleColor(titleColorForHighlighted, for: .highlighted)
                
            } else {
                
                animateScaleButton(scale: 1.0)
                backgroundColor = colorForNormal
                setTitleColor(titleColorForNormal, for: .normal)
            }
        }
    }
    
    
    func animateScaleButton(scale:CGFloat)
    {
        UIView.beginAnimations("ActionButtonScale", context: nil)
        UIView.setAnimationDuration(0.2)
        self.transform = CGAffineTransform(scaleX: scale,y: scale);
        UIView.commitAnimations()
    }
    
    
    func setBackgroundColor(_ color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()?.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
}

