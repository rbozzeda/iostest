//
//  UINavigationController+Extension.swift
//  iOSTest
//

import Foundation
import UIKit

extension UINavigationController
{
    open override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.navigationBar.shadowImage = UIImage.imageWithColor(.clear)
        self.navigationBar.tintColor = UIColor.red
        self.navigationBar.setBackgroundImage(UIImage.imageWithColor(.white), for: .any, barMetrics: .default)
        self.navigationBar.isTranslucent = false
        
        let attributes = [NSAttributedStringKey.font: UIFont.DINProMedium(size: 16)]
        self.navigationBar.titleTextAttributes = attributes
    }
}
