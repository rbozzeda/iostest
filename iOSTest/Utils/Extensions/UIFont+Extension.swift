//
//  UIFont+Extension.swift
//  iOSTest
//

import Foundation
import UIKit

extension UIFont
{
    public class func DINProMedium(size:CGFloat) -> UIFont
    {
        return UIFont(name: "DINPro-Medium", size: size)!
    }
}
