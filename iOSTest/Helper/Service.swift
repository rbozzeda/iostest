//
//  Service.swift
//  iOSTest
//

import Foundation
import Moya
import RxSwift

enum Service {
    case getMovies(page:Int)
    case getMovieDetail(id: Int)
    case searchMovies(text: String, page: Int)
}

func errorDebug(error: Error) -> MoyaError {
    if let moyaError = error as? MoyaError {
        print("[Error] Path:\(String(describing: moyaError.response?.response?.url?.path)), Code: \(String(describing: moyaError.response?.statusCode)), Descripition: \(String(describing: moyaError.localizedDescription))")
        return moyaError
    }
    return error as! MoyaError
}

extension Service: TargetType {
    var baseURL: URL { return URL(string: "https://api.themoviedb.org/3")! }
    var path: String {
        switch self {
        case .getMovies:
            return "/movie/now_playing"
        case .getMovieDetail(let id):
            return "/movie/\(id)"
        case .searchMovies:
            return "/search/movie"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getMovies, .getMovieDetail, .searchMovies:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .getMovieDetail:
            let params: [String: Any] = ["api_key": Constants.apiKey]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case .getMovies(let page):
            let params: [String: Any] = ["page": page,
                                         "api_key": Constants.apiKey]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case .searchMovies(let text, let page):
            let params: [String: Any] = ["query": text,
                                         "page": page,
                                         "api_key": Constants.apiKey]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getMovieDetail, .getMovies, .searchMovies:
            return "Half measures are as bad as nothing at all.".utf8Encoded
        }
    }
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
