//
//  Constants.swift
//  iOSTest
//

import Foundation

class Constants
{
    static let baseUrl  = "https://api.themoviedb.org/3"
    static let apiKey   = "9d79c2e6302d56d3a64c65bdb2c45334"
    
    static let imageURL = "https://image.tmdb.org/t/p/w500"
}
