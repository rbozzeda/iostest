//
//  MovieDetailPresenter.swift
//  iOSTest
//

import UIKit

protocol MovieDetailPresentationLogic
{
    func presentMovieDetail(response: MovieDetail.Fetch.Response)
    func presentError(error: Error)
}

class MovieDetailPresenter: MovieDetailPresentationLogic
{
    weak var viewController: MovieDetailDisplayLogic?
    
    
    func presentMovieDetail(response: MovieDetail.Fetch.Response)
    {
        let viewModel = MovieDetail.Fetch.ViewModel(movie: response.movie)
        viewController?.displayMovieDetails(viewModel: viewModel)
    }
    
    func presentError(error: Error)
    {
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        let alert = UIAlertController(title: "Oops!", message: "Algo errado não está certo", preferredStyle: .alert)
        alert.addAction(alertAction)
        
        viewController?.displayAlert(alert: alert)
    }
}
