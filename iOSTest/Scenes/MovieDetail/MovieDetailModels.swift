//
//  MovieDetailModels.swift
//  iOSTest
//

import UIKit

enum MovieDetail
{
    enum Fetch
    {
        struct Request
        {
            var movieId : Int
        }
        struct Response
        {
            var movie: Movie
        }
        struct ViewModel
        {
            var movie: Movie
        }
    }
}
