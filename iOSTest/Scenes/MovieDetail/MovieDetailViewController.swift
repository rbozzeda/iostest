//
//  MovieDetailViewController.swift
//  iOSTest
//

import UIKit
import JGProgressHUD

protocol MovieDetailDisplayLogic: class
{
    func displayMovieDetails(viewModel: MovieDetail.Fetch.ViewModel)
    func displayAlert(alert:UIAlertController)
}

class MovieDetailViewController: UIViewController, MovieDetailDisplayLogic
{
    var interactor: MovieDetailBusinessLogic?
    var router: (NSObjectProtocol & MovieDetailRoutingLogic & MovieDetailDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = MovieDetailInteractor()
        let presenter = MovieDetailPresenter()
        let router = MovieDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        getMovieDetail()
    }
    
    //MARK: Outlets & Vars
    @IBOutlet var backdrop:         UIImageView!
    @IBOutlet var poster:           UIImageView!
    @IBOutlet var movieTitle:       UILabel!
    @IBOutlet var movieDescription: UILabel!
    
    var progress : JGProgressHUD = JGProgressHUD(style: .dark)
    
    //MARK: Actions
    
    func getMovieDetail()
    {
        self.progress.show(in: view, animated: false)
        
        let request = MovieDetail.Fetch.Request(movieId: (router?.dataStore?.movie.id)!)
        interactor?.getMovieDetail(request: request)
    }
    
    func displayMovieDetails(viewModel: MovieDetail.Fetch.ViewModel)
    {
        self.progress.dismiss(animated: true)
        
        movieTitle.text = viewModel.movie.title
        movieDescription.text = viewModel.movie.overview
        
        // set image
        if  viewModel.movie.poster_path != nil {
            let imageURL = URL(string: Constants.imageURL + (viewModel.movie.poster_path)!)!
            self.poster.af_setImage(withURL: imageURL, placeholderImage: UIImage(), imageTransition: .crossDissolve(0.1))
        } else {
            self.poster.image = UIImage()
        }
        
        // set backdrop
        if  viewModel.movie.backdrop_path != nil {
            let imageURL = URL(string: Constants.imageURL + (viewModel.movie.backdrop_path)!)!
            self.backdrop.af_setImage(withURL: imageURL, placeholderImage: UIImage(), imageTransition: .crossDissolve(0.1))
        } else {
            self.backdrop.image = UIImage()
        }
    }
    
    func displayAlert(alert:UIAlertController)
    {
        self.progress.dismiss(animated: true)
        self.present(alert, animated: true, completion: nil)
    }
}
