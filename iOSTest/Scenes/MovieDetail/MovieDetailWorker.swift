//
//  MovieDetailWorker.swift
//  iOSTest
//

import UIKit
import Moya
import RxSwift

typealias responseHandlerMovieDetail = (_ response: MovieDetail.Fetch.Response) ->()

class MovieDetailWorker
{
    let bag = DisposeBag()
    
    func getMovieDetail(id:Int, success:@escaping(responseHandlerMovieDetail), fail:@escaping(Error) -> Void)
    {
        MovieManager.getMovieDetail(id: id).subscribe { (event) in
            switch event {
            case .next(let movie):
                success(MovieDetail.Fetch.Response(movie: movie))
            case .error(let error):
                fail(error)
            case .completed:
                print("Complete")
            }
            }.disposed(by: bag)
    }
}
