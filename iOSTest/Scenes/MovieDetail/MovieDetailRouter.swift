//
//  MovieDetailRouter.swift
//  iOSTest
//

import UIKit

@objc protocol MovieDetailRoutingLogic
{
}

protocol MovieDetailDataPassing
{
    var dataStore: MovieDetailDataStore? { get }
}

class MovieDetailRouter: NSObject, MovieDetailRoutingLogic, MovieDetailDataPassing
{
    weak var viewController: MovieDetailViewController?
    var dataStore: MovieDetailDataStore?
}
