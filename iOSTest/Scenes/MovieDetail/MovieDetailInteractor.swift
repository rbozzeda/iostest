//
//  MovieDetailInteractor.swift
//  iOSTest
//

import UIKit

protocol MovieDetailBusinessLogic
{
    func getMovieDetail(request: MovieDetail.Fetch.Request)
}

protocol MovieDetailDataStore
{
    var movie: Movie! { get set }
}

class MovieDetailInteractor: MovieDetailBusinessLogic, MovieDetailDataStore
{
    var presenter: MovieDetailPresentationLogic?
    var worker: MovieDetailWorker?
    
    var movie: Movie!
    
    func getMovieDetail(request: MovieDetail.Fetch.Request)
    {
        worker = MovieDetailWorker()
        worker?.getMovieDetail(id: request.movieId, success: { (response) in
            
            self.presenter?.presentMovieDetail(response: response)
            
        }, fail: { (error) in
            self.presenter?.presentError(error: error)
        })
    }
}
