//
//  MovieListModels.swift
//  iOSTest
//

import UIKit

enum MovieList
{
    enum Fetch
    {
        struct Request
        {
            var page : Int = 1
        }
        struct Response
        {
            var movies : [Movie]
        }
        struct ViewModel
        {
            var movies : [Movie]
        }
    }
}
