//
//  MovieListWorker.swift
//  iOSTest
//

import UIKit
import Moya
import RxSwift

typealias responseHandler = (_ response: MovieList.Fetch.Response) ->()

class MovieListWorker
{
    let bag = DisposeBag()
    
    func getMovies(page:Int, success:@escaping(responseHandler), fail:@escaping(Error) -> Void)
    {
        MovieManager.getMovies(page: page).subscribe { (event) in
            switch event {
            case .next(let movies):
                success(MovieList.Fetch.Response(movies: movies))
            case .error(let error):
                fail(error)
            case .completed:
                print("Complete")
            }
            }.disposed(by: bag)
    }
}
