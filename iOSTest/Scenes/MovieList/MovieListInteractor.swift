//
//  MovieListInteractor.swift
//  iOSTest
//

import UIKit

protocol MovieListBusinessLogic
{
    func getMovies(request: MovieList.Fetch.Request)
}

protocol MovieListDataStore
{
    var movies: [Movie] { get }
}

class MovieListInteractor: MovieListBusinessLogic, MovieListDataStore
{
    var movies: [Movie] = []
    
    var presenter: MovieListPresentationLogic?
    var worker: MovieListWorker?
    
    func getMovies(request: MovieList.Fetch.Request)
    {
        worker = MovieListWorker()
        worker?.getMovies(page: request.page, success: { (response) in
            
            for movie in response.movies.filter({ $0.vote_average! > 5.0 }) {
                self.movies.append(movie)
            }
            
            self.presenter?.presentMovies(response: MovieList.Fetch.Response(movies: self.movies))
            
        }, fail: { (error) in
            self.presenter?.presentError(error: error)
        })
    }
}
