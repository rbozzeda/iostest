//
//  MovieListViewController.swift
//  iOSTest
//

import UIKit
import UIScrollView_InfiniteScroll
import JGProgressHUD

protocol MovieListDisplayLogic: class
{
    func displayMovies(viewModel: MovieList.Fetch.ViewModel)
    func displayAlert(alert:UIAlertController)
}

class MovieListViewController: UIViewController, MovieListDisplayLogic
{
    var interactor: MovieListBusinessLogic?
    var router: (NSObjectProtocol & MovieListRoutingLogic & MovieListDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = MovieListInteractor()
        let presenter = MovieListPresenter()
        let router = MovieListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        getMovies()
        
        tableView.addInfiniteScroll { (tableView) -> Void in
            self.getMovies()
        }
    }

    //MARK: Outlets & Vars
    @IBOutlet var tableView : UITableView!
    
    var page: Int = 0
    var movies: [Movie] = []
    var progress : JGProgressHUD = JGProgressHUD(style: .dark)
    
    // MARK: Actions
    func getMovies()
    {
        self.progress.show(in: view, animated: false)
        
        page = page + 1
        let request = MovieList.Fetch.Request(page: page)
        interactor?.getMovies(request: request)
    }
    
    func displayMovies(viewModel: MovieList.Fetch.ViewModel)
    {
        self.progress.dismiss(animated: true)
        
        tableView.finishInfiniteScroll()
        
        self.movies = viewModel.movies
        self.tableView.reloadData()
    }
    
    func displayAlert(alert:UIAlertController)
    {
        self.progress.dismiss(animated: true)
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - Table View Data Source
extension MovieListViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as! MovieCell
        cell.movie = movies[indexPath.row]
        
        return cell
    }
}

//MARK: - Table View Delegate
extension MovieListViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let destination = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController")
        router?.routeToMovieDetail(segue: UIStoryboardSegue(identifier: "MovieDetailSegue", source: self, destination: destination))
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
