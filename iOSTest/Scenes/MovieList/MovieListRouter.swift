//
//  MovieListRouter.swift
//  iOSTest
//

import UIKit

@objc protocol MovieListRoutingLogic
{
    func routeToMovieDetail(segue: UIStoryboardSegue?)
}

protocol MovieListDataPassing
{
    var dataStore: MovieListDataStore? { get }
}

class MovieListRouter: NSObject, MovieListRoutingLogic, MovieListDataPassing
{
    weak var viewController: MovieListViewController?
    var dataStore: MovieListDataStore?
    
    // MARK: Routing
    
    func routeToMovieDetail(segue: UIStoryboardSegue?)
    {
        if let segue = segue {
            let destinationVC = segue.destination as! MovieDetailViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToMovieDetail(source: dataStore!, destination: &destinationDS)
            navigateToMovieDetail(source: viewController!, destination: destinationVC)
        }
    }
    
    // MARK: Navigation
    
    func navigateToMovieDetail(source: MovieListViewController, destination: MovieDetailViewController)
    {
        source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    
    func passDataToMovieDetail(source: MovieListDataStore, destination: inout MovieDetailDataStore)
    {
        let selectedRow = viewController?.tableView.indexPathForSelectedRow?.row
        destination.movie = source.movies[selectedRow!]
    }
}
