//
//  MovieListPresenter.swift
//  iOSTest
//

import UIKit

protocol MovieListPresentationLogic
{
    func presentMovies(response: MovieList.Fetch.Response)
    func presentError(error: Error)
}

class MovieListPresenter: MovieListPresentationLogic
{
    weak var viewController: MovieListDisplayLogic?
    
    
    func presentMovies(response: MovieList.Fetch.Response)
    {
        let viewModel = MovieList.Fetch.ViewModel(movies: response.movies)
        viewController?.displayMovies(viewModel: viewModel)
    }
    
    func presentError(error: Error)
    {
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        let alert = UIAlertController(title: "Oops!", message: "Algo errado não está certo", preferredStyle: .alert)
        alert.addAction(alertAction)
        
        viewController?.displayAlert(alert: alert)
    }
}
