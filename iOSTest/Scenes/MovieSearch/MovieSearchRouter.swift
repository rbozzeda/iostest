//
//  MovieSearchRouter.swift
//  iOSTest
//

import UIKit

@objc protocol MovieSearchRoutingLogic
{
    func routeToMovieDetail(segue: UIStoryboardSegue?)
}

protocol MovieSearchDataPassing
{
    var dataStore: MovieSearchDataStore? { get }
}

class MovieSearchRouter: NSObject, MovieSearchRoutingLogic, MovieSearchDataPassing
{
    weak var viewController: MovieSearchViewController?
    var dataStore: MovieSearchDataStore?
    
    // MARK: Routing
    
    func routeToMovieDetail(segue: UIStoryboardSegue?)
    {
        if let segue = segue {
            let destinationVC = segue.destination as! MovieDetailViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToMovieDetail(source: dataStore!, destination: &destinationDS)
            navigateToMovieDetail(source: viewController!, destination: destinationVC)
        }
    }
    
    // MARK: Navigation
    
    func navigateToMovieDetail(source: MovieSearchViewController, destination: MovieDetailViewController)
    {
        source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    
    func passDataToMovieDetail(source: MovieSearchDataStore, destination: inout MovieDetailDataStore)
    {
        let selectedRow = viewController?.tableView.indexPathForSelectedRow?.row
        destination.movie = source.movies[selectedRow!]
    }
}
