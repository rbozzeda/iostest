//
//  MovieSearchViewController.swift
//  iOSTest
//

import UIKit
import JGProgressHUD

protocol MovieSearchDisplayLogic: class
{
    func displayMovies(viewModel: MovieSearch.Fetch.ViewModel)
    func displayAlert(alert:UIAlertController)
}

class MovieSearchViewController: UITableViewController, MovieSearchDisplayLogic
{
    var interactor: MovieSearchBusinessLogic?
    var router: (NSObjectProtocol & MovieSearchRoutingLogic & MovieSearchDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = MovieSearchInteractor()
        let presenter = MovieSearchPresenter()
        let router = MovieSearchRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.addInfiniteScroll { (tableView) -> Void in
            self.getMovies()
        }
    }
    
    
    //MARK: Outlets & Vars
    @IBOutlet var searchBar : UISearchBar!
    
    var page = 0
    var movies: [Movie] = []
    var progress : JGProgressHUD = JGProgressHUD(style: .dark)
    
    //MARK: Actions
    func getMovies()
    {
        self.progress.show(in: view, animated: true)
        
        page = page + 1
        let request = MovieSearch.Fetch.Request(page: page, text: searchBar.text!)
        interactor?.searchMovies(request: request)
    }
    
    func displayMovies(viewModel: MovieSearch.Fetch.ViewModel) {
        
        self.progress.dismiss(animated: true)
        
        tableView.finishInfiniteScroll()
        
        self.movies = viewModel.movies
        self.tableView.reloadData()
    }
    
    func displayAlert(alert:UIAlertController)
    {
        self.progress.dismiss(animated: true)
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - Table View Data Source
extension MovieSearchViewController
{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = movies[indexPath.row].title
        return cell
    }
}

//MARK: - Table View Delegate
extension MovieSearchViewController
{
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let destination = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController")
        router?.routeToMovieDetail(segue: UIStoryboardSegue(identifier: "MovieSearchSegue", source: self, destination: destination))
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK: - Search Bar Delegate
extension MovieSearchViewController : UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.page = 0
        
        self.getMovies()
        self.tableView.tableFooterView = UIView()
    }
}

