//
//  MovieSearchWorker.swift
//  iOSTest
//

import UIKit
import Moya
import RxSwift

typealias responseHandlerSearch = (_ response: MovieSearch.Fetch.Response) ->()

class MovieSearchWorker
{
    let bag = DisposeBag()
    
    func searchMovies(text: String, page:Int, success:@escaping(responseHandlerSearch), fail:@escaping(Error) -> Void)
    {
        
        MovieManager.searchMovies(text: text, page: page).subscribe { (event) in
            switch event {
            case .next(let movies):
                success(MovieSearch.Fetch.Response(movies: movies))
            case .error(let error):
                fail(error)
            case .completed:
                print("Complete")
            }
            }.disposed(by: bag)
    }
}
