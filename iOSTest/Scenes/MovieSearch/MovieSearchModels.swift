//
//  MovieSearchModels.swift
//  iOSTest
//

import UIKit

enum MovieSearch
{
    enum Fetch
    {
        struct Request
        {
            var page : Int = 1
            var text : String
        }
        struct Response
        {
            var movies : [Movie]
        }
        struct ViewModel
        {
            var movies : [Movie]
        }
    }
}

