//
//  MovieSearchPresenter.swift
//  iOSTest
//

import UIKit

protocol MovieSearchPresentationLogic
{
    func presentMovies(response: MovieSearch.Fetch.Response)
    func presentError(error: Error)
}

class MovieSearchPresenter: MovieSearchPresentationLogic
{
    weak var viewController: MovieSearchDisplayLogic?
    
    
    func presentMovies(response: MovieSearch.Fetch.Response)
    {
        let viewModel = MovieSearch.Fetch.ViewModel(movies: response.movies)
        viewController?.displayMovies(viewModel: viewModel)
    }
    
    func presentError(error: Error)
    {
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        let alert = UIAlertController(title: "Oops!", message: "Algo errado não está certo", preferredStyle: .alert)
        alert.addAction(alertAction)
        
        viewController?.displayAlert(alert: alert)
    }
}
