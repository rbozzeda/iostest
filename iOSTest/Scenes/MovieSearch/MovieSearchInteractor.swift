//
//  MovieSearchInteractor.swift
//  iOSTest
//

import UIKit

protocol MovieSearchBusinessLogic
{
    func searchMovies(request: MovieSearch.Fetch.Request)
}

protocol MovieSearchDataStore
{
    var movies: [Movie] { get }
}

class MovieSearchInteractor: MovieSearchBusinessLogic, MovieSearchDataStore
{
    var movies: [Movie] = []
    
    var presenter: MovieSearchPresentationLogic?
    var worker: MovieSearchWorker?
    
    func searchMovies(request: MovieSearch.Fetch.Request)
    {
        if request.page == 1 { // clear history
            self.movies.removeAll(keepingCapacity: true)
        }
        
        worker = MovieSearchWorker()
        worker?.searchMovies(text: request.text, page: request.page, success: { (response) in
            
            for movie in response.movies.filter({ $0.vote_average! > 5.0 }) {
                self.movies.append(movie)
            }
            
            self.presenter?.presentMovies(response: MovieSearch.Fetch.Response(movies: self.movies))
            
        }, fail: { (error) in
            self.presenter?.presentError(error: error)
        })
    }
}
