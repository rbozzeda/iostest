//
//  MovieManager.swift
//  iOSTest
//

import Foundation
import Moya
import RxSwift

public class MovieManager
{
    static let provider = MoyaProvider<Service>()
    
    /// Get list of in theater movies
    ///
    /// - Parameter page: Page number
    /// - Returns: List of movies
    class func getMovies(page: Int) -> Observable<[Movie]>
    {
        return self.provider.rx.request(.getMovies(page: page))
            .asObservable()
            .filterSuccessfulStatusCodes()
            .catchError({ (error) -> Observable<Response> in
                throw errorDebug(error: error)
            })
            .map([Movie].self, atKeyPath: "results", using: JSONDecoder.init(), failsOnEmptyData: false)
    }
    
    
    /// Get movie details
    ///
    /// - Parameter id: Movie id
    /// - Returns: Return movie object
    class func getMovieDetail(id: Int) -> Observable<Movie>
    {
        return self.provider.rx.request(.getMovieDetail(id: id))
            .asObservable()
            .filterSuccessfulStatusCodes()
            .catchError({ (error) -> Observable<Response> in
                throw errorDebug(error: error)
            })
            .map(Movie.self)
    }
    
    
    /// Search movies by title
    ///
    /// - Parameters:
    ///   - text: Title of movie
    ///   - page: Page number
    /// - Returns: List of movies
    class func searchMovies(text: String, page: Int) -> Observable<[Movie]>
    {
        return self.provider.rx.request(.searchMovies(text: text, page: page))
            .asObservable()
            .filterSuccessfulStatusCodes()
            .catchError({ (error) -> Observable<Response> in
                throw errorDebug(error: error)
            })
            .map([Movie].self, atKeyPath: "results", using: JSONDecoder.init(), failsOnEmptyData: false)
    }
}
